import numpy as np
import pandas as pd
from IPython.core.display import display, HTML
from pandas import DataFrame as DataFrame_pd


def __fix_types_pprint(x):
    try:
        x.str
        return x
    except:
        try:
            if 'int' in str(x.dtype):
                return x.apply(lambda x: f'{x:,}')
            return x.astype(np.float64)
        except:
            return x


def __insert_style(background=None, color=None):
    """
    Returns css style
    :param background: string
    :param color: string
    :return: HTML style
    """
    background = background or 'white'
    color = color or 'black'
    style = """
        <style scoped>
            .dataframe-div {
                overflow: auto;
                position: relative;
            }

            .dataframe-div .dataframe thead th {
                position: -webkit-sticky; /* for Safari */
                position: sticky;
                top: 0;
                background: """+background+""";
                color: """+color+""";
            }

            .dataframe-div .dataframe thead th:first-child {
                left: 0;
                z-index: 1;
            }

            .dataframe-div .dataframe tbody tr th:only-of-type {
                    vertical-align: middle;
                }

            .dataframe-div .dataframe tbody tr th {
                position: sticky;
                left: 0;
                background: """+background+""";
                color: """+color+""";
                vertical-align: top;
            }

            }
        </style>
        """
    return HTML(style)

def add_pprint(limit=5, background='#00734d', color='white', max_columns=500):
    pd.set_option('display.max_columns', max_columns)
    pd.set_option('display.width', 1000)
    pd.options.display.float_format = '{:20,.3f}'.format

    def pprint(self, limit=5, index=None, size=300):
        size = str(size)
        if index is None:
            df_html = self.head(limit).apply(__fix_types_pprint, axis=0).to_html()
        else:
            df_html = (self.head(limit).set_index(index)
                        .rename_axis(index=None, columns=None).rename_axis(index, axis=1)
                        .apply(__fix_types_pprint, axis=0).to_html())
        df_html = '<div class="dataframe-div" style="max-height: {0}px;">{1}\n</div>'.format(size, df_html)
        return display(HTML(df_html))
    DataFrame_pd.pprint = pprint

    display(__insert_style(background, color))
